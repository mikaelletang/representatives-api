<?php

namespace App\Representative\Domain;

interface DepartmentalCounsellorRepositoryInterface
{
    public function getForInseeCode(InseeCode $inseeCode) : ?DepartmentalCounsellor;
}
