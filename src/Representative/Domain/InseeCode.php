<?php

namespace App\Representative\Domain;

class InseeCode
{
    /** @var string */
    private $code;

    public function __construct($code)
    {
        $this->code = sprintf('%02d', $code);

    }

    public function getCode(): string
    {
        return $this->code;
    }

}
