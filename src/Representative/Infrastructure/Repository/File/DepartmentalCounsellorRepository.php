<?php

namespace App\Representative\Infrastructure\Repository\File;

use App\Representative\Domain\DepartmentalCounsellor;
use App\Representative\Domain\DepartmentalCounsellorRepositoryInterface;
use App\Representative\Domain\InseeCode;

class DepartmentalCounsellorRepository implements DepartmentalCounsellorRepositoryInterface
{

    private const DATA_SOURCE = 'presidents_conseils_generaux.csv';

    /** @var DepartmentalCounsellor[] */
    private $departmentalCouncellors;

    public function __construct()
    {
        $fileLocation = sprintf("%s/%s", dirname(__FILE__), self::DATA_SOURCE);

        $this->departmentalCouncellors = [];
        if (($handle = fopen($fileLocation, "r")) !== false) {
            while (($councellor = fgetcsv($handle, 1000, ";")) !== false) {
                list($departmentCode, $departmentName, $lastName, $firstName, $sex) = $councellor;
                $inseeCode = new InseeCode($departmentCode);
                $this->departmentalCouncellors[$inseeCode->getCode()] = new DepartmentalCounsellor($firstName, ucfirst(strtolower($lastName)));
            }
            fclose($handle);
        }
    }

    public function getForInseeCode(InseeCode $inseeCode): ?DepartmentalCounsellor
    {
        return array_key_exists($inseeCode->getCode(), $this->departmentalCouncellors) ? $this->departmentalCouncellors[$inseeCode->getCode()] : null;
    }
}
