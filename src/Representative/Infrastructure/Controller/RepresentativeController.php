<?php

namespace App\Representative\Infrastructure\Controller;

use App\Representative\Domain\DepartmentalCounsellorRepositoryInterface;
use App\Representative\Domain\InseeCode;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RepresentativeController
{
    /**
     * @var DepartmentalCounsellorRepositoryInterface
     */
    private $departmentalCounsellorRepository;

    public function __construct(DepartmentalCounsellorRepositoryInterface $departmentalCounsellorRepository) {

        $this->departmentalCounsellorRepository = $departmentalCounsellorRepository;
    }

    /**
     * @Route("/api/departmental-counsellor")
     */
    public function getRepresentativeCounsellor(Request $request = null) : Response
    {
        $inseeCode = new InseeCode($request->get('insee_code'));
        $councilor = $this->departmentalCounsellorRepository->getForInseeCode($inseeCode);
        return new Response(json_encode($councilor));
    }
}
