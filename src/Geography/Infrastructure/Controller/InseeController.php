<?php

namespace App\Geography\Infrastructure\Controller;

use Http\Client\HttpClient;
use Http\Message\MessageFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InseeController
{

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var HttpClient
     */
    private $httpClient;

    public function __construct(MessageFactory $messageFactory, HttpClient $httpClient)
    {
        $this->messageFactory = $messageFactory;
        $this->httpClient = $httpClient;
    }

    /**
     * @Route("/api/geography/insee")
     */
    public function getInseeCodesForAddress(Request $request): Response
    {
        $address = $request->get('address');
        $request = $this->messageFactory->createRequest('GET', 'https://api-adresse.data.gouv.fr/search/?q='.$address);
        $response = $this->httpClient->sendRequest($request);
        $json = json_decode($response->getBody()->getContents(), $asArray = true);
        $cityCode = $json['features'][0]['properties']['citycode'];
        $context = $json['features'][0]['properties']['context'];
        [$departmentCode, $departmentName, $regionName] = explode(", ", $context);

        

        $payload = [
            'city_code' => $cityCode,
            'department_code' => $departmentCode,
            'region_code' => '84'
        ];
        return new Response(json_encode($payload));

    }
}
