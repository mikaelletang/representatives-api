<?php

namespace App\Tests\Representative\Infrastructure\Repository\Remote;

use App\Representative\Domain\InseeCode;
use App\Representative\Infrastructure\Repository\File\DepartmentalCounsellorRepository;
use PHPUnit\Framework\TestCase;

class DepartmentalCouncellorRepositoryTest extends TestCase
{
    public function testGetForInseeCode()
    {
        $departmentalCounsellorRepository = new DepartmentalCounsellorRepository();
        $councellor = $departmentalCounsellorRepository->getForInseeCode($inseeCode = new InseeCode('33'));
        $this->assertEquals('Gleyze', $councellor->lastName);
    }
}
