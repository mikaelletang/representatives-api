<?php

namespace App\Tests\Representative\Infrastructure\Controller;

use App\Representative\Infrastructure\Controller\RepresentativeController;
use App\Representative\Infrastructure\Repository\File\DepartmentalCounsellorRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class RepresentativeControllerTest extends TestCase
{

    private $departmentalCounsellorRepository;

    protected function setup()
    {
        parent::setup();
        $this->departmentalCounsellorRepository = new DepartmentalCounsellorRepository();
    }


    public function testGetDepartmentalCounsellor()
    {
        $controller = new RepresentativeController($this->departmentalCounsellorRepository);
        $params = ['insee_code' => 33];
        $response = $controller->getRepresentativeCounsellor(new Request($params));
        $counsellor = json_decode($response->getContent(), $asArray = true);
        var_dump($counsellor);
        $this->assertEquals('Gleyze', $counsellor['lastName']);
    }
}
