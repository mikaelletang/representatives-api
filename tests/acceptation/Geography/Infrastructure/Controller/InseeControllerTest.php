<?php

namespace App\Tests\Geography\Infrastructure\Controller;

use App\Geography\Infrastructure\Controller\InseeController;
use App\Representative\Infrastructure\Controller\RepresentativeController;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class InseeControllerTest extends WebTestCase
{
    /** @var ContainerInterface */
    private $container;

    public function setup()
    {
        parent::setUp();
    }


    public function testGetInseeCodesForAddress()
    {
        $client = static::createClient();
        $address = 'Les Chalayes 07130 Toulaud';
        $client->request('GET', '/api/geography/insee?address='.$address);
        $details = json_decode($client->getResponse()->getContent(), $asArray = true);
        $this->assertEquals('07323', $details['city_code']);
        $this->assertEquals('07', $details['department_code']);
        $this->assertEquals('84', $details['region_code']);
    }
}
