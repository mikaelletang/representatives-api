<?php

namespace App\Representative\Domain;

use PHPUnit\Framework\TestCase;

class InseeCodeTest extends TestCase
{

    public function setup()
    {

    }

    public function testWithCodeAsString()
    {
        $inseeCode = new InseeCode('07');
        $this->assertSame('07', $inseeCode->getCode());
    }

    public function testWithCodeAsNumber()
    {
        $inseeCode = new InseeCode(33);
        $this->assertSame('33', $inseeCode->getCode());
    }

    public function testWithCodeAsShortString()
    {
        $inseeCode = new InseeCode('7');
        $this->assertSame('07', $inseeCode->getCode());
    }


    public function testWithCodeAsLongString()
    {
        $inseeCode = new InseeCode('999');
        $this->assertSame('999', $inseeCode->getCode());
    }
}
